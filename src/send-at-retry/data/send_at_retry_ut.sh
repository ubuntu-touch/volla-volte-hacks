#!/bin/sh -ex
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
set -x
dump_name="$1"

chmod +x /mnt/tmp/android_run.sh
chmod +x /tmp/lxc_attach.sh

sudo /tmp/lxc_attach.sh /mnt/tmp/android_run.sh "$1" 2>&1 \
	| tee /tmp/"$dump_name"
