#!/bin/sh -ex
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
# Related: https://github.com/ubports/ubuntu-touch/issues/1836

LD_PRELOAD= LD_LIBRARY_PATH= lxc-attach -n android -e -- "$@"
