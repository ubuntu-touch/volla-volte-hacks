#!/bin/sh -e
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>

DEV="/dev/radio/ptty2cmd2"
RET="/data/local/tmp/ims_apn"

# $1: AT command to send
# $2: retry until finding this string in reply
send() {
	echo "---"
	echo "> $1"

	rm -rf "$RET"
	cat "$DEV" > "$RET" &
	pid="$!"

	for i in $(seq 1 5); do
		printf .
		echo "$1" > "$DEV"
		sleep 0.1

		if grep -q "$2" "$RET"; then
			cat "$RET"
			kill "$pid"
			return
		fi
	done

	kill "$pid"
	echo
	echo "ERROR: could not find '$2' in response:"
	cat "$RET"
	exit 1
}

# Query firmware information
send 'AT+CGMI' '+CGMI:'
send 'AT+CGMM' '+CGMM:'
send 'AT+CGMR' '+CGMR:'

# Add your at commands below

echo
echo "---"
echo "done"
