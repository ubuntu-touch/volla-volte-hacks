Send AT commands multiple times until an expected string is in the reply.

This hack is a PoC for sending AT commands through the same pseudo TTY that
mtkfusionrild is using.

Preparation:
- Ubuntu touch with ssh enabled and writable rootfs
