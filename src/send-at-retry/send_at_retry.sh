#!/bin/sh -ex
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
dump_name="$(date "+%Y-%m-%d_%H%M%S")"

adb push data/send_at_retry_ut.sh /tmp/run.sh
adb push data/lxc_attach.sh /tmp/lxc_attach.sh
adb push data/send_at_retry_android.sh /mnt/tmp/android_run.sh

adb forward tcp:8023 tcp:22
ssh -t -p 8023 phablet@localhost \
	sh -e /tmp/run.sh "$dump_name"

mkdir -p dump/"${dump_name}_at"
adb pull /tmp/"$dump_name" "dump/${dump_name}_at/${dump_name}.txt"

ln -sf "$PWD/dump/${dump_name}_at/${dump_name}.txt" /tmp/latest.txt
