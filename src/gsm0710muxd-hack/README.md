gsm0710muxd runs together with mtkfusionrild on the volla phone. Research
showed that it is GPL2 licensed, but the real source code could not yet be
found. Until it is available, this was an approach to get similar source code
running and use it instead of the binary gsm0710muxd.

This code is imported from
[here](https://github.com/s0rd3s/a3300_opensource/tree/master/mediatek/hardware/gsm0710muxd),
with patching on top to make it build without Android NDK, and to behave more
like the binary version we have.
    
Upstream of gsm0710muxd seems to be from pyneo, but it seems that there's no
git/svn repository with proper history. My guess is that the source where this
was forked from could be found in debian's source package archives.

Related code, also without complete history, but was forked from common code
at some point:
[freesmartphone/gsm0710muxd](https://github.com/freesmartphone/gsm0710muxd)
