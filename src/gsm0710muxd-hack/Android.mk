# MediaTek Inc. (C) 2010. All rights reserved.
# Copyright 2008 Texas Instruments
#
#Author(s) Mikkel Christensen (mlc@ti.com) and Ulrik Bech Hald (ubh@ti.com)

#
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE:= gsm0710muxd

LOCAL_SRC_FILES:= \
	src/gsm0710muxd.c \
	src/gsm0710muxd_fc.c \

LOCAL_SHARED_LIBRARIES := \
	libcutils

ifeq ($(TELEPHONY_DFOSET),yes)
LOCAL_SHARED_LIBRARIES += libdfo
endif

LOCAL_CFLAGS := \
	-DMUX_ANDROID \
	-D__CCMNI_SUPPORT__ \
	-D__MUXD_FLOWCONTROL__ \

ifeq ($(GEMINI),yes)
	LOCAL_CFLAGS += -DMTK_GEMINI
else
    ifneq ($(MTK_SHARE_MODEM_SUPPORT),1)
   LOCAL_CFLAGS += -DMTK_GEMINI 
  endif
endif


ifeq ($(MTK_DT_SUPPORT),yes)
    LOCAL_CFLAGS += -DMTK_DT_SUPPORT
endif


ifneq ($(MTK_INTERNAL),yes)
	LOCAL_CFLAGS += -D__PRODUCTION_RELEASE__
endif

ifeq ($(MTK_VT3G324M_SUPPORT),yes)
  LOCAL_CFLAGS += -D__ANDROID_VT_SUPPORT__
endif

ifeq ($(MT6280_SUPER_DONGLE),yes)
  LOCAL_CFLAGS += -DPOLL_MODEM_LONGER
endif

LOCAL_CFLAGS += -DMTK_RIL_MD1

LOCAL_C_INCLUDES := $(KERNEL_HEADERS) \
	$(TOPDIR)/hardware/libhardware_legacy/include \
	$(TOPDIR)/hardware/libhardware/include \
	$(MTK_PATH_SOURCE)/external/dfo/featured/ \
	$(TARGET_OUT_HEADERS)/dfo/

LOCAL_LDLIBS := -lpthread

include $(BUILD_EXECUTABLE)

#===========================
include $(CLEAR_VARS)

LOCAL_MODULE:= gsm0710muxdmd2

LOCAL_SRC_FILES:= \
	src/gsm0710muxd.c \
	src/gsm0710muxd_fc.c \

LOCAL_SHARED_LIBRARIES := \
	libcutils

ifeq ($(TELEPHONY_DFOSET),yes)
LOCAL_SHARED_LIBRARIES += libdfo
endif

LOCAL_CFLAGS := \
	-DMUX_ANDROID \
	-D__CCMNI_SUPPORT__ \
	-D__MUXD_FLOWCONTROL__ \

ifeq ($(GEMINI),yes)
	LOCAL_CFLAGS += -DMTK_GEMINI
else
    ifneq ($(MTK_SHARE_MODEM_SUPPORT),1)
   LOCAL_CFLAGS += -DMTK_GEMINI 
  endif
endif

ifeq ($(MTK_DT_SUPPORT),yes)
    LOCAL_CFLAGS += -DMTK_DT_SUPPORT
endif


ifneq ($(MTK_INTERNAL),yes)
	LOCAL_CFLAGS += -D__PRODUCTION_RELEASE__
endif

ifeq ($(MTK_VT3G324M_SUPPORT),yes)
  LOCAL_CFLAGS += -D__ANDROID_VT_SUPPORT__
endif

LOCAL_CFLAGS += -DMTK_RIL_MD2

LOCAL_C_INCLUDES := $(KERNEL_HEADERS) \
	$(TOPDIR)/hardware/libhardware_legacy/include \
	$(TOPDIR)/hardware/libhardware/include \
	$(MTK_PATH_SOURCE)/external/dfo/featured/ \
	$(TARGET_OUT_HEADERS)/dfo/
                    
LOCAL_LDLIBS := -lpthread

include $(BUILD_EXECUTABLE)
