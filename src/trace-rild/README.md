Scripts to run strace + tcpdump on mtkfusionrild and to extract AT commands
from the strace dump.

Preparation:
- Rooted Android
- Termux installed, sshd enabled and running
- strace, tsu, tcpdump installed

Usage:
1. run dump.sh
2. run parse_strace.py to get AT commands form the strace dump
