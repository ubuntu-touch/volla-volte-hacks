#!/bin/sh -e
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>
#
# dump.sh copies this file to Android and runs it

rild="mtkfusionrild"
strsize="1000"
dump_name="$1"
pid="$(sudo ps ax | grep "$rild" | grep -v grep | head -n1 | awk '{print $1}')"
outdir="/mnt/sdcard/Download/dump/$1"

clean_up() {
	sudo killall -9 strace tcpdump || true 2>/dev/null
}

if [ -d "$outdir" ]; then
	echo "ERROR: outdir already exists: $outdir"
	exit 1
fi

if [ -z "$pid" ]; then
	echo "ERROR: failed to get pid of $rild"
	exit 1
fi

mkdir -p "$outdir"
set -x

clean_up

sudo tcpdump \
	-U \
	-i any \
	-n not port 8022 \
	-s0 \
	-w "$outdir/$1.pcap" \
	&

sudo strace \
	-f \
	-o "$outdir/$1_$rild.strace" \
	-p "$pid" \
	-r \
	-s "$strsize" \
	-ttt \
	-v \
	-y \
	&

sleep 1

set +x
echo
echo
echo "rild: $rild (pid: $pid)"
echo "Capturing to: $outdir"
echo "Press return to stop capturing"
echo
echo
read foo

set -x
clean_up
echo "Capture stopped"
