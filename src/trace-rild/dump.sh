#!/bin/sh -ex
# SPDX-License-Identifier: GPL-2.0-or-later
# Author: Oliver Smith
# Copyright 2021 sysmocom - s.f.m.c. GmbH <info@sysmocom.de>

dump_name="$(date "+%Y-%m-%d_%H%M%S")"

adb push dump_run_on_phone.sh /mnt/sdcard/Download/
adb forward tcp:8022 tcp:8022
ssh -p 8022 localhost \
	sh -e /mnt/sdcard/Download/dump_run_on_phone.sh "$dump_name"

mkdir -p dump
adb pull /mnt/sdcard/Download/dump/"$dump_name" dump/"$dump_name"
